package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type DesocntosEncargos struct {
	TipoRegistro                  string  `json:"TipoRegistro"`
	PercentualDescontoFinanceiro  float32 `json:"PercentualDescontoFinanceiro"`
	ValorDescontoFinanceiro       float64 `json:"ValorDescontoFinanceiro"`
	PercentualDescontoCOmercial   float32 `json:"PercentualDescontoCOmercial"`
	ValorDescontoCOmercial        float32 `json:"ValorDescontoCOmercial"`
	PercentualDescontoPromocional float32 `json:"PercentualDescontoPromocional"`
	ValorDescontoPromocional      float64 `json:"ValorDescontoPromocional"`
	PercentualEncargosFinanceiros float32 `json:"PercentualEncargosFinanceiros"`
	ValorEncargosFinanceiros      float64 `json:"ValorEncargosFinanceiros"`
	PercentualEncargosFrete       float32 `json:"PercentualEncargosFrete"`
	ValorEncargosFrete            float64 `json:"ValorEncargosFrete"`
	PercentualEncargosSeguro      float32 `json:"PercentualEncargosSeguro"`
	ValorEncargosSeguro           float64 `json:"ValorEncargosSeguro"`
}

func (d *DesocntosEncargos) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesDesocntosEncargos

	err = posicaoParaValor.ReturnByType(&d.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualDescontoFinanceiro, "PercentualDescontoFinanceiro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorDescontoFinanceiro, "ValorDescontoFinanceiro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualDescontoCOmercial, "PercentualDescontoCOmercial")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorDescontoCOmercial, "ValorDescontoCOmercial")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualDescontoPromocional, "PercentualDescontoPromocional")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorDescontoPromocional, "ValorDescontoPromocional")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualEncargosFinanceiros, "PercentualEncargosFinanceiros")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorEncargosFinanceiros, "ValorEncargosFinanceiros")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualEncargosFrete, "PercentualEncargosFrete")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorEncargosFrete, "ValorEncargosFrete")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualEncargosSeguro, "PercentualEncargosSeguro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorEncargosSeguro, "ValorEncargosSeguro")
	if err != nil {
		return err
	}

	return err
}

var PosicoesDesocntosEncargos = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":                  {0, 2, 0},
	"PercentualDescontoFinanceiro":  {2, 7, 2},
	"ValorDescontoFinanceiro":       {7, 22, 2},
	"PercentualDescontoCOmercial":   {22, 27, 2},
	"ValorDescontoCOmercial":        {27, 42, 2},
	"PercentualDescontoPromocional": {42, 47, 2},
	"ValorDescontoPromocional":      {47, 62, 2},
	"PercentualEncargosFinanceiros": {62, 67, 2},
	"ValorEncargosFinanceiros":      {67, 82, 2},
	"PercentualEncargosFrete":       {82, 87, 2},
	"ValorEncargosFrete":            {87, 102, 2},
	"PercentualEncargosSeguro":      {102, 107, 2},
	"ValorEncargosSeguro":           {107, 122, 2},
}
