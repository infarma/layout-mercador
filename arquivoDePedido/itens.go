package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Itens struct {
	TipoRegistro                              string  `json:"TipoRegistro"`
	NumeroSequencialLinhaItem                 int32   `json:"NumeroSequencialLinhaItem"`
	NumeroItemPedido                          int32   `json:"NumeroItemPedido"`
	QuanlificadorAlteracao                    string  `json:"QuanlificadorAlteracao"`
	TipoCodigoProduto                         string  `json:"TipoCodigoProduto"`
	CodigoProduto                             string  `json:"CodigoProduto"`
	DescricaoProduto                          string  `json:"DescricaoProduto"`
	RefenrenciaProduto                        string  `json:"RefenrenciaProduto"`
	UnidadeMedida                             string  `json:"UnidadeMedida"`
	NumeroUnidadesConsumoEmbalagemPedido      int32   `json:"NumeroUnidadesConsumoEmbalagemPedido"`
	QuantidadePedida                          float64 `json:"QuantidadePedida"`
	QuantidadeBonificada                      float64 `json:"QuantidadeBonificada"`
	QuantidadeTroca                           float64 `json:"QuantidadeTroca"`
	TipoEmbalagem                             string  `json:"TipoEmbalagem"`
	NumeroEmbalagens                          int32   `json:"NumeroEmbalagens"`
	ValorBrutoLinhaItem                       float64 `json:"ValorBrutoLinhaItem"`
	ValorLiquidoLinhaItem                     float64 `json:"ValorLiquidoLinhaItem"`
	PrecoBrutoUnitario                        float64 `json:"PrecoBrutoUnitario"`
	PrecoLiquidoUnitario                      float64 `json:"PrecoLiquidoUnitario"`
	BasePrecoUnitario                         int32   `json:"BasePrecoUnitario"`
	UnidadeMedidaBasePrecoUnitario            string  `json:"UnidadeMedidaBasePrecoUnitario"`
	ValorUnitarioDescontoComercial            float64 `json:"ValorUnitarioDescontoComercial"`
	PercentualDescontoComercial               float32 `json:"PercentualDescontoComercial"`
	ValorUnitarioIPI                          float64 `json:"ValorUnitarioIPI"`
	AliquotaIPI                               float32 `json:"AliquotaIPI"`
	ValorUnitarioDespesaAcessoriaTributada    float64 `json:"ValorUnitarioDespesaAcessoriaTributada"`
	ValorUnitarioDespesaAcessoriaNaoTributada float64 `json:"ValorUnitarioDespesaAcessoriaNaoTributada"`
	ValorEncargoFrete                         float64 `json:"ValorEncargoFrete"`
	PercentualDescontoRepasseICMS             float32 `json:"PercentualDescontoRepasseICMS"`
	DescontoComercial                         float32 `json:"DescontoComercial"`
}

func (i *Itens) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesItens

	err = posicaoParaValor.ReturnByType(&i.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.NumeroSequencialLinhaItem, "NumeroSequencialLinhaItem")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.NumeroItemPedido, "NumeroItemPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.QuanlificadorAlteracao, "QuanlificadorAlteracao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.TipoCodigoProduto, "TipoCodigoProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.CodigoProduto, "CodigoProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.DescricaoProduto, "DescricaoProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.RefenrenciaProduto, "RefenrenciaProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.UnidadeMedida, "UnidadeMedida")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.NumeroUnidadesConsumoEmbalagemPedido, "NumeroUnidadesConsumoEmbalagemPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.QuantidadePedida, "QuantidadePedida")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.QuantidadeBonificada, "QuantidadeBonificada")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.QuantidadeTroca, "QuantidadeTroca")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.TipoEmbalagem, "TipoEmbalagem")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.NumeroEmbalagens, "NumeroEmbalagens")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.ValorBrutoLinhaItem, "ValorBrutoLinhaItem")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.ValorLiquidoLinhaItem, "ValorLiquidoLinhaItem")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.PrecoBrutoUnitario, "PrecoBrutoUnitario")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.PrecoLiquidoUnitario, "PrecoLiquidoUnitario")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.BasePrecoUnitario, "BasePrecoUnitario")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.UnidadeMedidaBasePrecoUnitario, "UnidadeMedidaBasePrecoUnitario")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.ValorUnitarioDescontoComercial, "ValorUnitarioDescontoComercial")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.PercentualDescontoComercial, "PercentualDescontoComercial")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.ValorUnitarioIPI, "ValorUnitarioIPI")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.AliquotaIPI, "AliquotaIPI")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.ValorUnitarioDespesaAcessoriaTributada, "ValorUnitarioDespesaAcessoriaTributada")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.ValorUnitarioDespesaAcessoriaNaoTributada, "ValorUnitarioDespesaAcessoriaNaoTributada")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.ValorEncargoFrete, "ValorEncargoFrete")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.PercentualDescontoRepasseICMS, "PercentualDescontoRepasseICMS")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.DescontoComercial, "DescontoComercial")
	if err != nil {
		return err
	}

	return err
}

var PosicoesItens = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":                              {0, 2, 0},
	"NumeroSequencialLinhaItem":                 {2, 6, 0},
	"NumeroItemPedido":                          {6, 11, 0},
	"QuanlificadorAlteracao":                    {11, 14, 0},
	"TipoCodigoProduto":                         {14, 17, 0},
	"CodigoProduto":                             {17, 31, 0},
	"DescricaoProduto":                          {31, 71, 0},
	"RefenrenciaProduto":                        {71, 91, 0},
	"UnidadeMedida":                             {91, 94, 0},
	"NumeroUnidadesConsumoEmbalagemPedido":      {94, 99, 0},
	"QuantidadePedida":                          {99, 114, 2},
	"QuantidadeBonificada":                      {114, 129, 2},
	"QuantidadeTroca":                           {129, 144, 2},
	"TipoEmbalagem":                             {144, 147, 0},
	"NumeroEmbalagens":                          {147, 152, 0},
	"ValorBrutoLinhaItem":                       {152, 167, 2},
	"ValorLiquidoLinhaItem":                     {167, 182, 2},
	"PrecoBrutoUnitario":                        {182, 197, 2},
	"PrecoLiquidoUnitario":                      {197, 212, 2},
	"BasePrecoUnitario":                         {212, 217, 0},
	"UnidadeMedidaBasePrecoUnitario":            {217, 220, 0},
	"ValorUnitarioDescontoComercial":            {220, 235, 2},
	"PercentualDescontoComercial":               {235, 240, 2},
	"ValorUnitarioIPI":                          {240, 255, 2},
	"AliquotaIPI":                               {255, 260, 2},
	"ValorUnitarioDespesaAcessoriaTributada":    {260, 275, 2},
	"ValorUnitarioDespesaAcessoriaNaoTributada": {275, 290, 2},
	"ValorEncargoFrete":                         {290, 305, 2},
	"PercentualDescontoRepasseICMS":             {305, 309, 2},
	"DescontoComercial":                         {309, 313, 2},
}
